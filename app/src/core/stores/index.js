import { legacy_createStore } from "redux";
import globalReducer from "../reducers";

export const store = legacy_createStore(globalReducer)