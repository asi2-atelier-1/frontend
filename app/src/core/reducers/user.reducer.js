// User Actions
export const userActions = {
    login: "@@user/LOGIN",
    logout: "@@user/LOGOUT",
    changeAccount: "@@user/CHANGE_ACCOUNT",
    updateAccount: "@@user/UPDATE_ACCOUNT"
}

export const login = (user) => {
    return {
        type: userActions.login,
        payload: {
            userId: user.id,
            isLoggedIn: true,
            userName: user.surName + " " + user.lastName,
            userEmail: user.email,
            userLogin: user.login,
            userToken: user.token,
            userAccount: user.account
        }
    }
}

export const logout = () => {
    return {
        type: userActions.logout,
    }
}

export const changeAccount = (amount) => {
    return {
        type: userActions.changeAccount,
        payload: amount
    }
}

export const updateAccount = (newAmount) => {
    return {
        type: userActions.updateAccount,
        payload: newAmount
    }
}

// User Selectors

export const selectUserId = (state) => state.userReducer.userId
export const selectUserName = (state) => state.userReducer.userName
export const selectUserMoney = (state) => state.userReducer.userAccount
export const selectIsUserLoggedIn = (state) => state.userReducer.isLoggedIn
export const selectUserToken = (state) => state.userReducer.userToken

// User Reducer

const initialState = {
    userId: null,
    userEmail: "",
    userName: "",
    userLogin: "",
    isLoggedIn: false,
    userToken: null,
    userAccount: 0,
}

const userReducer = (state = initialState, action) => {
    switch(action.type) {
        case userActions.login:
            // console.log("REDUCER LOGIN", action)
            return {
                ...action.payload
            }
        case userActions.logout:
            // console.log("REDUCER LOGOUT")
            return initialState
        case userActions.changeAccount:
            // console.log("REDUCER change account")
            return {
                ...state,
                userAccount: state.userAccount + action.payload
            }
        case userActions.updateAccount:
            // console.log("REDUCER update account")
            return {
                ...state,
                userAccount: action.payload
            }
        default:
            return state
    }
}

export default userReducer