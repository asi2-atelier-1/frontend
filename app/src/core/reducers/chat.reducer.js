// Actions
export const chatActions = {
    addMessage: "@@chat/ADD_MESSAGE",
    initMessages: "@@chat/INIT_MESSAGES",
    setChatPal: "@@chat/SET_CHAT_PAL",
    addNewUser: "@@chat/ADD_NEW_USER",
    removeUser: "@@chat/REMOVE_USER",
    initConnectedUsers: "@@chat/INIT_USERS",
}

export const addMessage = (message) => {
    return {
        type: chatActions.addMessage,
        payload: {
            ...message
        }
    }
}

export const initMessages = (messages) => {
    return {
        type: chatActions.initMessages,
        payload: [
            ...messages
        ]
    }
}

export const setChatPal = (chatPal) => {
    return {
        type: chatActions.setChatPal,
        payload: {
            ...chatPal
        }
    }
}

export const addNewUser = (newUserInfo) => {
    return {
        type: chatActions.addNewUser,
        payload: {
            ...newUserInfo
        }
    }
}

export const initConnectedUsers = (connectedUsers) => {
    return {
        type: chatActions.initConnectedUsers,
        payload: [
            ...connectedUsers
        ]
    }
}


export const removeUser = (userToRemoveInfo) => {
    return {
        type: chatActions.removeUser,
        payload: {
            ...userToRemoveInfo
        }
    }
}

// Selectors
export const selectMessages = (state) => state.chatReducer.messages
export const selectChatPal = (state) => state.chatReducer.chatPal
export const selectConnectedUserList = (state) => state.chatReducer.connectedUserList

// Reducer
const initialState = {
    messages: [],
    chatPal: {},
    connectedUserList: []
}

const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case chatActions.addMessage:
            console.log("REDUCER ADD MESSAGE", action)
            return {
                ...state,
                messages: [...state.messages, action.payload]
            }
        case chatActions.initMessages:
            console.log("REDUCER INIT MESSAGES", action)
            return {
                ...state,
                messages: [...state.messages, ...action.payload]
            }
        case chatActions.setChatPal:
            console.log("REDUCER SET CHAT PAL", action)
            return {
                ...state,
                chatPal: { ...action.payload }
            }
        case chatActions.addNewUser:
            console.log("REDUCER ADD NEW USER", action)
            return {
                ...state,
                connectedUserList: [...state.connectedUserList, action.payload]
            }
        case chatActions.removeUser:
            console.log("REDUCER REMOVE USER", action)
            return {
                ...state,
                connectedUserList: [...state.connectedUserList.filter(u => u.id !== action.payload.id)]
            }
        case chatActions.initConnectedUsers:
            return {
                ...state,
                connectedUserList: [...action.payload]
            }
        default:
            return state
    }
}

export default chatReducer