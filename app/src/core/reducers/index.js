import { combineReducers } from "redux"

import userReducer from "./user.reducer"
import chatReducer from "./chat.reducer"
import cardTransactionReducer from "./cardTransaction.reducer"
import gameReducer from "./game.reducer"
import cardReducer from "./card.reducer"

const globalReducer = combineReducers({
    userReducer,
    chatReducer,
    cardTransactionReducer,
    gameReducer,
    cardReducer,
})

export default globalReducer