// Actions

export const cardTransactionActions = {
    initCardList: "@@cardTransaction/INIT_CARDS",
    cardTransactionDone: "@@cardTransaction/TRANSACTION_DONE",
    cardTransactionPending: "@@cardTransaction/TRANSACTION_PENDING",
    setSelectedCard: "@@cardTransaction/SET_SELECTED_CARD",
    updatePendingTransaction: "@@CardTransaction/UPDATE_PENDING"
}

export const initCardList = (cardList, transactionType) => {
    return {
        type: cardTransactionActions.initCardList,
        payload: {
            cardList: cardList,
            transactionType: transactionType
        }
    }
}

export const cardTransactionDone = (cardInstanceId, transactionType) => {
    return {
        type: cardTransactionActions.cardTransactionDone,
        payload: {
            cardInstanceId: cardInstanceId,
            transactionType: transactionType
        }
    }
}

export const cardTransactionPending = (cardInstanceId, transactionType) => {
    return {
        type: cardTransactionActions.cardTransactionPending,
        payload: {
            cardInstanceId: cardInstanceId,
            transactionType: transactionType
        }
    }
}

export const setSelectedCard = (cardInstanceId, transactionType) => {
    return {
        type: cardTransactionActions.setSelectedCard,
        payload: {
            cardInstanceId: cardInstanceId,
            transactionType: transactionType
        }
    }
}

export const updateTransactionPending = (pendingList) => {
    return {
        type: cardTransactionActions.updatePendingTransaction,
        payload: pendingList
    }
}

// Selectors
export const selectToSellCardList = (state) => state.cardTransactionReducer.toSellCardList
export const selectToBuyCardList = (state) => state.cardTransactionReducer.toBuyCardList

export const selectSelectedCardSell = (state) => {
    const idToFind = state.cardTransactionReducer.selectedCardIdSell

    return idToFind
        ? state.cardTransactionReducer.toSellCardList?.find(card => card.idCardInstance === idToFind)
        : null
}

export const selectSelectedCardBuy = (state) => {
    const idToFind = state.cardTransactionReducer.selectedCardIdBuy

    return idToFind
        ? state.cardTransactionReducer.toBuyCardList?.find(card => card.idCardInstance === idToFind)
        : null
}

export const selectSelectedCardSellPending = (state) => {
    const cardInstance = selectSelectedCardSell(state)
    return cardInstance?.isPending
}

export const selectSelectedCardBuyPending = (state) => {
    const cardInstance = selectSelectedCardBuy(state)
    return cardInstance?.isPending
}


// Reducer
const initialState = {
    toSellCardList: null,
    toBuyCardList: null,
    selectedCardIdSell: null,
    selectedCardIdBuy: null
}

const setCardList = (cards) => {
    let returnList = []
    for (const card of cards) {
        returnList.push({...card, isPending: false})
    }
    console.log("INIT LIST STATE", returnList)
    return returnList
}

const setCardPending = (idCardInstance, list) => {
    const cardInstance = list.find(card => card.idCardInstance === idCardInstance)
    console.log("SETTING CARD PENDING", cardInstance)
    const returnList = [...list.filter(card => card.idCardInstance !== idCardInstance), {...cardInstance, isPending: true}]
    return returnList
}

const setCardTransactionDone = (idCardInstance, list) => {
    return [...list.filter(card => card.idCardInstance !== idCardInstance)]
}

const mergePendingTransactionWithCardList = (toSellCardList, toBuyCardList, pendingList) => {
    if (pendingList == null) return
    const pendingCardInstanceIds = pendingList.map(c => c.idCardInstance)
    const deletedSell = []
    const deletedBuy = []
    const filteredSell = toSellCardList?.filter(card => {
        if (! card.isPending
            || pendingCardInstanceIds.includes(card.idCardInstance)
        ) return true
        deletedSell.push(card.idCardInstance)
        return false
    }) || null
    
    const filteredBuy = toBuyCardList?.filter(card => {
        if (! card.isPending
            || pendingCardInstanceIds.includes(card.idCardInstance)
        ) return true
        deletedBuy.push(card.idCardInstance)
        return false
    }) || null
    
    return [filteredSell, filteredBuy, deletedSell, deletedBuy]
}

const cardTransactionReducer = (state = initialState, action) => {
    const list = action.payload?.transactionType === 'BUY' ? state.toBuyCardList : state.toSellCardList
    const listName = action.payload?.transactionType === 'BUY' ? "toBuyCardList": "toSellCardList"
    const selectCardName = action.payload?.transactionType === "BUY" ? "selectedCardIdBuy" : "selectedCardIdSell"
    
    switch(action.type) {
        case cardTransactionActions.initCardList:
            // console.log("INIT CARDTRANSACTION " + listName, action.payload)
            return {
                ...state,
                [listName]: setCardList(action.payload.cardList, list)
            }
        case cardTransactionActions.cardTransactionPending:
            // console.log("PENDING CARDTRANSACTION " + listName, action.payload)
            return {
                ...state,
                [listName]: setCardPending(action.payload.cardInstanceId, list)
            }
        case cardTransactionActions.cardTransactionDone:
            // console.log("DONE CARDTRANSACTION " + listName, action.payload)
            return {
                ...state,
                [listName]: setCardTransactionDone(action.payload.cardInstanceId, list)
            }
        case cardTransactionActions.setSelectedCard:
            return {
                ...state,
                [selectCardName]: action.payload.cardInstanceId
            }
        case cardTransactionActions.updatePendingTransaction:
            // console.log("UPDATING TRANSACTION", state.toSellCardList, state.toBuyCardList)
            const [filteredSell, filteredBuy, deletedSell, deletedBuy] =
                mergePendingTransactionWithCardList(
                    state.toSellCardList,
                    state.toBuyCardList,
                    action.payload
                )
            
            if (deletedSell.length === 0 && deletedBuy.length === 0) {
                return state
            }

            let returnState = {
                ...state,
            }

            if (deletedSell.length !== 0) {
                const selectedCardIdSell = deletedSell.includes(state.selectedCardIdSell) ? null : state.selectedCardIdSell 
                returnState = {
                    ...returnState,
                    toSellCardList: filteredSell,
                    selectedCardIdSell: selectedCardIdSell
                }
            }
            if (deletedBuy.length !== 0) {
                const selectedCardIdBuy = deletedBuy.includes(state.selectedCardIdBuy) ? null : state.selectedCardIdBuy
                returnState = {
                    ...returnState,
                    toBuyCardList: filteredBuy,
                    selectedCardIdBuy: selectedCardIdBuy
                }
            }

            return returnState
        default:
            return state
    }
}

export default cardTransactionReducer