// Actions
export const cardActions = {
    addCard: '@@card/ADD'
}

export const addCard = (card) => {
    return {
        type: cardActions.addCard,
        payload: card
    }
}

// Selectors

export const selectCardById = id => state => {
    const card = state.cardReducer.cardList?.find(c => c.idCard === id)
    if (card === null)
        return null
    return card
}


// Reducer
const initialState = {
    cardList: []
}

const cardReducer = (state = initialState, action) => {
    switch(action.type) {
        case cardActions.addCard:
            return {
                ...state,
                cardList: [
                    ...state.cardList,
                    action.payload
                ]
            }
        default:
            return state
    }
}

export default cardReducer