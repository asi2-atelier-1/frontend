// Actions

export const gameActions = {
    update: "@@game/UPDATE",
    selectTargetCard: "@@game/SELECT_TARGET_CARD",
    selectSourceCard: "@@game/SELECT_SOURCE_CARD",
}

export const update = (game) => {
    return {
        type: gameActions.update,
        payload: game
    }
}

export const selectTargetCard = (card) => {
    return {
        type: gameActions.selectTargetCard,
        payload: card
    }
}

export const selectSourceCard = (card) => {
    return {
        type: gameActions.selectSourceCard,
        payload: card
    }
}

// Selectors

export const selectGame = (state) => state.gameReducer.game;
export const selectedTargetCard = (state) => state.gameReducer.targetCard;
export const selectedSourceCard = (state) => state.gameReducer.sourceCard;

export const selectUser = (state) => selectGame(state).users[state.userReducer.userId];
export const selectUserCards = (state) => selectUser(state).cards;

export const selectOtherUser = (state) => Object.values(selectGame(state).users).find(u => u.id !== state.userReducer.userId);
export const selectOtherUserCards = (state) => selectOtherUser(state).cards;

export const selectIsUserTurn = (state) => state.userReducer.userId === selectGame(state).turnUser;

// Reducer

const initialState = {
    game: {
        id: null,
        users: {},
        status: null,
        win: null,
        turn: null
    },
    targetCard: null,
    sourceCard: null
}

const gameReducer = (state = initialState, action) => {
    switch(action.type) {
        case gameActions.update:
            console.log("UPDATE GAME", action.payload)
            return {
                ...state,
                game: { ...action.payload }
            }
        case gameActions.selectTargetCard:
            return {
                ...state,
                targetCard: { ...action.payload }
            }
        case gameActions.selectSourceCard:
            return {
                ...state,
                sourceCard: { ...action.payload }
            }
        default:
            return state
    }
}

export default gameReducer;