import { Grid } from "semantic-ui-react"
import CardPreviewSmall from "../Cards/CardPreviewSmall.component"
import { useEffect, useState } from "react";

import { useSelector } from "react-redux";
import { selectIsUserTurn } from "../../core/reducers/game.reducer";

function PlayerBoard({ cardList = [], onSelectCard }) {
  const isUserTurn = useSelector(selectIsUserTurn);
  const [selectedCard, setSelectedCard] = useState(undefined);

  useEffect(() => {
    if(!isUserTurn) {
      setSelectedCard(undefined);
    }
  }, [isUserTurn, selectedCard])

  const onClickCardPreview = (card) => {
    console.log(card)

    if(!isUserTurn)
      return;

    setSelectedCard(card);
    onSelectCard(card);
  }

  return (
    <Grid style={{ display: "flex", flexDirection: "column", justifyContent: "flex-start" }}>
      <Grid.Row>
        {cardList.map((c, index) => {
          return <Grid.Column width={4} key={index}>
            <CardPreviewSmall
              isSelected={c.cardInstance.idCardInstance === selectedCard?.idCardInstance}
              cardInstance={c.cardInstance}
              handleOnClick={() => onClickCardPreview(c.cardInstance)}
            />
          </Grid.Column>
        })}
      </Grid.Row>
    </Grid>
  )
}

export default PlayerBoard;
