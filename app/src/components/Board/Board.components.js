import { Grid, Divider } from "semantic-ui-react";
import PlayerBoard from "./PlayerBoard.components";

import { useSelector, useDispatch } from "react-redux";
import { selectUserCards, selectOtherUserCards, selectSourceCard, selectTargetCard } from "../../core/reducers/game.reducer";

function Board(props) {
    const dispatch = useDispatch();

    const userCards = Object.values(useSelector(selectUserCards));
    const onSelectUserCard = (card) => {
        console.log(card)
        dispatch(selectSourceCard(card));
    }

    const otherUserCards = Object.values(useSelector(selectOtherUserCards));
    const onSelectOtherUserCard = (card) => {
        console.log(card)
        dispatch(selectTargetCard(card));
    }
    
    return (
        <Grid.Row>
            <Grid.Row>
                <PlayerBoard cardList={userCards} onSelectCard={onSelectUserCard}/>
            </Grid.Row>
            <Divider horizontal>VS</Divider>
            <Grid.Row>
                <PlayerBoard cardList={otherUserCards} onSelectCard={onSelectOtherUserCard}/>
            </Grid.Row>
        </Grid.Row>
    )
}

export default Board;