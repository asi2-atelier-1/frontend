import { useSelector } from "react-redux";
import { Grid } from "semantic-ui-react";
import { selectedSourceCard, selectedTargetCard } from "../../core/reducers/game.reducer";
import CardPreview from "../Cards/CardPreview.component";

export default function BoardCardPreview() {
  const selectedCard = useSelector(selectedSourceCard)
  const selectedOpponentCard = useSelector(selectedTargetCard)
  
  return (
    <>
      <Grid.Row>
          {selectedCard ?
            <CardPreview 
              cardInstance={selectedCard}
              isPending={true}
              priceText=""
              handleCardTransaction={() => {}}
            /> : null
          }
      </Grid.Row>
      <Grid.Row>
          {selectedOpponentCard ?
            <CardPreview 
              cardInstance={selectedOpponentCard}
              isPending={true}
              priceText=""
              handleCardTransaction={() => {}}
            /> : null
          }
      </Grid.Row>
    </>
  )
}
