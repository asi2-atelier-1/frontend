import { Outlet } from "react-router-dom"
import { Container } from "semantic-ui-react"
import Header from "./Header/Header.component"

function Layout() {

    return (
        <div>
            <Header />
            
            <Container id="main" style={{width: "100%"}}> 
                <Outlet />
            </Container>
        </div>
    )
}

export default Layout