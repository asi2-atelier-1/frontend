import { Container, Menu, Image, Button } from 'semantic-ui-react'
import { useDispatch, useSelector } from "react-redux"
import { UserAvatar, UserMoney } from "../User/UserInfo.component"

import { selectIsUserLoggedIn, selectUserMoney, selectUserName, logout } from '../../core/reducers/user.reducer'
import { MenuDropDown } from './MenuDropDown.component'
import { Link } from 'react-router-dom'
import { PRIVATE_ROUTES_FULL } from '../../Routes/Routes'

function Header() {

    const isLoggedIn = useSelector(selectIsUserLoggedIn)
    const userMoney = useSelector(selectUserMoney)
    const userName = useSelector(selectUserName)

    const dispatch = useDispatch()
    const handleLogout = () => {
        dispatch(logout())
    }

    return (
        <>
            <Menu attached="top" inverted
                style={{borderBottom: "solid 1px grey", backgroundColor: "#6f6f6f"}}
                >
                <Container style={{width: "100%"}}>
                    {isLoggedIn
                        ? <MenuDropDown />
                        : null
                    }
                    <Menu.Item header>
                        <Link to={PRIVATE_ROUTES_FULL.HOME}>
                            <Image size="mini" src="/images/card-game.svg" style={{ marginRight: "1.5em" }} />
                        </Link>
                        Card Market
                    </Menu.Item>
                    {isLoggedIn
                        ?
                            <Menu.Item>
                                <UserMoney money={userMoney}/>
                            </Menu.Item>
                        : null
                    }
                    {isLoggedIn
                        ?
                            <Menu.Item position='right'>
                                <div style={{marginRight: "15px"}}>
                                    <UserAvatar name={userName} />
                                </div>
                                <div>
                                    <Button icon="log out" content="LOGOUT" onClick={handleLogout}/>
                                </div>
                            </Menu.Item>
                        : null
                    }
                    {/* {isLoggedIn
                        ?
                            <Menu.Item position='right'>
                            </Menu.Item>
                        : null
                    } */}
                </Container>
            </Menu>
        </>
    )
}

export default Header