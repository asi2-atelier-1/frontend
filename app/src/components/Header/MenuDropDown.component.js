import { Link } from 'react-router-dom'
import { Icon, Dropdown } from 'semantic-ui-react'

import { PRIVATE_ROUTES_FULL } from '../../Routes/Routes'

export const MenuDropDown = () => {
    return (
        <Dropdown icon="bars" item simple>
            <Dropdown.Menu>
                <Dropdown.Item>
                    <Link to={PRIVATE_ROUTES_FULL.HOME} style={{color: "black"}}>
                        <Icon name="home" />
                        HOME
                    </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                    <Link to={PRIVATE_ROUTES_FULL.BUY} style={{color: "black"}}>
                        <Icon name="dollar" />
                        BUY Cards
                    </Link>
                </Dropdown.Item>
                <Dropdown.Item>
                    <Link to={PRIVATE_ROUTES_FULL.SELL} style={{color: "black"}}>
                        <Icon name="dollar" />
                        SELL Cards
                    </Link>
                </Dropdown.Item>
            </Dropdown.Menu>
            
        </Dropdown>
    )
}