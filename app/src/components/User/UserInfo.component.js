import { Icon } from "semantic-ui-react"

function UserInfo({ icon, info }) {
    return (
        <div>
            <Icon name={icon} />
            <span style={{marginLeft: "5px", fontSize: "1.2em"}}>
                {info}
            </span>
        </div>
    )
}

export const UserAvatar = ({ name }) =>  <UserInfo icon="user" info={name} />
export const UserMoney = ({ money }) =>  <UserInfo icon="dollar" info={money} />