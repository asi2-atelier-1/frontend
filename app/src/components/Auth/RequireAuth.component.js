import { useEffect } from "react"
import { Outlet, useNavigate } from "react-router-dom"

import { PUBLIC_ROUTES_FULL } from "../../Routes/Routes"
import { selectIsUserLoggedIn } from "../../core/reducers/user.reducer" 
import { useSelector } from "react-redux"

const RequireAuth = () => {
    
    const auth = useSelector(selectIsUserLoggedIn)

    const navigate = useNavigate()
    useEffect(() => {
        if (!auth) {
            navigate(PUBLIC_ROUTES_FULL.LOGIN, {replace: true})
        }
    }, [auth, navigate])


    return (
        auth
            ? <Outlet />
            : null
    )
}

export default RequireAuth