import { useEffect } from "react"
import { useSelector } from "react-redux"
import { Outlet, useNavigate } from "react-router-dom"

import { PRIVATE_ROUTES_FULL } from "../../Routes/Routes"
import { selectIsUserLoggedIn } from "../../core/reducers/user.reducer" 

const RequireNoAuth = () => {
    
    const auth = useSelector(selectIsUserLoggedIn)

    console.log("No AUTH Component: ", auth)
    const navigate = useNavigate()
    useEffect(() => {
        if (auth) {
            console.log("GOING BACK TO HOMEPAGE")
            navigate(PRIVATE_ROUTES_FULL.HOME, {replace: true})
        }
    }, [auth, navigate])


    return (
        !auth
            ? <Outlet />
            : null
    )
}

export default RequireNoAuth