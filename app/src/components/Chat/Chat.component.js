import { Grid } from "semantic-ui-react"

import ChatHeader from "./ChatHeader.component"
import ChatUserSelector from "./ChatUserSelector.component"
import ChatContainer from "./ChatContainer.component"
import ChatInput from "./ChatInput.component"

import { selectMessages, selectConnectedUserList } from "../../core/reducers/chat.reducer"
import { selectUserId } from "../../core/reducers/user.reducer";
import { useSelector } from 'react-redux';


function Chat() {
  const idUser = useSelector(selectUserId);
  const connectedUsers = [{id: -1, surName: "All", lastName: ""}, ...useSelector(selectConnectedUserList)];
  const connectedUsersDict = connectedUsers.reduce((a, b) => (a[b.id] = b, a), {});

  const listMessage = useSelector(selectMessages);
  listMessage.forEach(m => {
    if(m.destId !== undefined) {
      m.destination = connectedUsersDict[m.destId];
      m.timestamp = m.time
    }
    if(m.srcId !== undefined) {
      m.source = connectedUsersDict[m.srcId];
      m.timestamp = m.time
    }
  })

  return (
    <Grid>
      <ChatHeader userName="Eriau" />
      <ChatUserSelector userList={connectedUsers.filter(u => u.id !== idUser)} />
      <ChatContainer listMessage={listMessage} />
      <ChatInput />
    </Grid>
  )
}

export default Chat
