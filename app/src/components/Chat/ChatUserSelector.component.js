import { Grid, Select } from "semantic-ui-react"
import { useDispatch } from 'react-redux';
import { setChatPal } from "../../core/reducers/chat.reducer";

const getOptions = (userList) => {
  const options = []
  for (let i=0; i<userList.length; i++){
    options.push({"key": userList[i].id, "value":i , "text": `${userList[i].lastName} ${userList[i].surName}`})
  }
  return options
}

function ChatUserSelector({userList}) {
  const options = getOptions(userList)
  const dispatch = useDispatch()

  const handleOnChange = (e, d) => {
    dispatch(setChatPal(userList[d.value]));
  }

  return (
    <Grid.Row>
      <Grid.Column width={14}>
        <Select onChange={(e, d) => handleOnChange(e, d)} placeholder='Select user' fluid options={options} />
      </Grid.Column>
    </Grid.Row>
  )
}

export default ChatUserSelector
