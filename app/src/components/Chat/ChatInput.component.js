import { Grid, Form, TextArea, Button, Icon } from "semantic-ui-react"
import ws from "../../ws"
import { useRef } from "react";

function ChatInput() {
    const handleOnClick = () => {
        ws.sendMessage(textArea.current.ref.current.value);
    }

    const textArea = useRef();

    return (
        <Grid.Row>
            <Grid.Column width={11}>
                <Form>
                    <TextArea placeholder='Tell us more' ref={textArea}/>
                </Form>      
            </Grid.Column>
            <Grid.Column width={1}>
                <Button onClick={handleOnClick} style={{height : "100%"}} primary>
                    <Icon name="location arrow"/>
                </Button>
            </Grid.Column>
        </Grid.Row>
    )
}

export default ChatInput
