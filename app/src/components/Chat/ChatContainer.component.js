import { Grid } from "semantic-ui-react"
import { useEffect, useState } from "react"

import ChatMessage from "./ChatMessage.component"
import { selectUserId } from "../../core/reducers/user.reducer";
import { selectChatPal } from "../../core/reducers/chat.reducer";
import { useSelector } from "react-redux";

function ChatContainer({listMessage}) {
  const idUser = useSelector(selectUserId);
  const idPal = useSelector(selectChatPal)?.id;
  const [getListMessageToDisplay, setListMessageToDisplay] = useState([]);

  useEffect(() => {
    console.log("hello")
    setListMessageToDisplay(
      idPal !== -1 
        ? listMessage.filter(m => [m.destination?.id, m.source?.id].includes(idPal)) 
        : listMessage
    );
    // setListMessageToDisplay(listMessage);
  }, [idUser, idPal, listMessage]);

  return (
    <Grid.Row>
      <Grid.Column width={12}>
        <Grid style={{height: "50vh", alignContent : "flex-start"}}>
          {getListMessageToDisplay.map(lm => <ChatMessage text={lm.text} userName={lm.source.surName} key={lm.timestamp}/>)}
        </Grid>
      </Grid.Column>
    </Grid.Row>
  )
}


export default ChatContainer
