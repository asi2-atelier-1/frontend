import { Grid } from "semantic-ui-react"
import { UserAvatar } from "../User/UserInfo.component"

function ChatHeader({userName}) {
  return (
    <Grid.Row>
        <Grid.Column width={10}>
           Chat
        </Grid.Column>
        <Grid.Column width={6}>
            <UserAvatar name={userName} />
        </Grid.Column>
    </Grid.Row>
  )
}

export default ChatHeader
