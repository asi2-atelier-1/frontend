import { Grid, Label } from "semantic-ui-react"

function ChatMessage({text, userName}) {
  return (
    <Grid.Row style={{maxHeight : "80px"}}>
      <Grid.Column>
        <div>
          <Label as='a' image>
              <img src='/images/card-game.svg' alt="chat user icon" />
              {userName}
          </Label>
          <div>{text}</div>
        </div>
      </Grid.Column>
    </Grid.Row>
  )
}

export default ChatMessage
