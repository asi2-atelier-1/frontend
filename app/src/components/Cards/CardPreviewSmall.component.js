import useFetchCard from "../../hooks/useFetchCard"

function CardPreviewSmall({ cardInstance, isSelected = false, handleOnClick }) {

    const {hp, energy, attack} = cardInstance
    const cardModel = useFetchCard(cardInstance.idCard)
    if(cardModel == null)
        return null
    const {name, smallImgUrl} = cardModel


    return (
        <div className="ui special cards" onClick={() => handleOnClick(cardInstance)}>
            <div className={`card ${isSelected ? "card-selected" : ""}`}>
                <div className="content">
                        <div className="ui grid">
                            <div className="three column row">
                                <div className="column" >
                                    <h5>{name}</h5>
                                </div>
                                <div className="column" style={{textAlign: "center"}}>
                                    <span className="ui green circular label">{Math.round(hp)}</span>
                                </div>
                                <div className="column" style={{textAlign: "center"}}>
                                    <span className="ui red circular label">{Math.round(attack)}</span>
                                </div>
                                <div className="column" style={{textAlign: "center"}}>
                                    <span className="ui yellow circular label">{Math.round(energy)}</span>
                                </div>
                            </div>
                        </div>
                </div>
                <div className="image imageCard">
                    <div className="ui fluid image">
                        <img id="cardImgId" className="ui centered image" src={smallImgUrl} alt="card" />
                    </div>
                </div>   
            </div>
        </div>
    )
}

export default CardPreviewSmall