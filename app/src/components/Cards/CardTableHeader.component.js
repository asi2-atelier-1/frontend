import { Table } from "semantic-ui-react"

function CardTableHeader() {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>ID</Table.HeaderCell>
                <Table.HeaderCell>Energy</Table.HeaderCell>
                <Table.HeaderCell>HP</Table.HeaderCell>
                <Table.HeaderCell>Defence</Table.HeaderCell>
                <Table.HeaderCell>Attack</Table.HeaderCell>
                <Table.HeaderCell>Price</Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    )
}

export default CardTableHeader