import { useEffect, useState } from "react"
import { Loader, Message } from "semantic-ui-react"
import { getCardById } from "../../api/private/apiCards"

function CardPreview({ cardInstance, isPending, priceText, handleCardTransaction }) {

    const { energy, hp, defence, attack, price } = cardInstance
    const [apiState, setApiState] = useState({
        card: null,
        isLoading: false,
        error: null
    })

    const { card, isLoading, error } = apiState

    useEffect(() => {
        const getCards = async (id) => {
            try {
                setApiState({ card: null, isLoading: true, error: null })
                const response = await getCardById(id)
                if (response !== null) {
                    setApiState({ card: response, isLoading: false, error: null })
                }
            } catch (error) {
                setApiState({ card: null, isLoading: false, error: error })
            }
        }

        getCards(cardInstance.idCard)
    }, [cardInstance.idCard])

    if (isLoading) return <Loader active inline >Loading</Loader>
    else {
        if (error) {
            return (<Message error>Error while loading Card preview</Message>)
        } else if (card !== null) {
            const { family, name, smallImgUrl, description } = card
            return (
                <div className="ui special cards">
                    <div className="card">

                        <div className="content">
                            <div className="ui grid">
                                <div className="three column row">
                                    <div className="column">
                                        <i className="heart outline icon"></i><span id="cardHPId">{Math.round(hp)}</span>
                                    </div>
                                    <div className="column">
                                        <h5>{family}</h5>
                                    </div>
                                    <div className="column">
                                        <span id="energyId">{Math.round(attack)}</span>
                                        <i className="lightning icon"></i>
                                    </div>
                                    <div className="column">
                                        <span id="energyId">{Math.round(energy)}</span>
                                        <i className="lightning icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="image imageCard">

                            <div className="blurring dimmable image">
                                {/* <div className="ui inverted dimmer">
                                    <div className="content">
                                        <div className="center">
                                            <div className="ui primary button">Add Friend</div>
                                        </div>
                                    </div>
                                </div> */}
                                <div className="ui fluid image">
                                    <span className="ui left corner label">
                                        {name}
                                    </span>
                                    <img id="cardImgId" className="ui centered image" src={smallImgUrl} alt="card" />
                                </div>
                            </div>
                        </div>
                        <div className="content">
                            <div className="ui form tiny">
                                <div className="field">
                                    <label id="cardNameId"></label>
                                    <textarea
                                        id="cardDescriptionId"
                                        className="overflowHiden"
                                        readOnly
                                        rows="2"
                                        value={description}
                                    >
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div className="content">
                            <i className="heart outline icon"></i><span id="cardHPId"> HP {Math.round(hp)}</span>
                            <div className="right floated ">
                                <span id="cardEnergyId">Energy {Math.round(energy)}</span>
                                <i className="lightning icon"></i>
                            </div>
                        </div>
                        <div className="content">
                            <span className="right floated">
                                <span id="cardAttackId"> Attack {Math.round(attack)}</span>
                                <i className=" wizard icon"></i>
                            </span>
                            <i className="protect icon"></i>
                            <span id="cardDefenceId">Defense {Math.round(defence)}</span>
                        </div>
                        <div className={`ui bottom attached button ${isPending ? "disabled" : ""}`} onClick={handleCardTransaction}>
                            <i className="money icon"></i>
                            {priceText} for <span id="cardPriceId"> {price}$</span>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null
        }

    }
}

export default CardPreview