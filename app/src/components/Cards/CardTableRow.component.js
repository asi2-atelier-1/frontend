import { Table } from "semantic-ui-react"

function CardTableRow({id, energy, hp, defence, attack, price, handleOnClick}) {
    return (
        <Table.Row onClick={() => handleOnClick(id)}>
            <Table.Cell textAlign="center">{id}</Table.Cell>
            <Table.Cell textAlign="center">{energy}</Table.Cell>
            <Table.Cell textAlign="center">{hp}</Table.Cell>
            <Table.Cell textAlign="center">{defence}</Table.Cell>
            <Table.Cell textAlign="center">{attack}</Table.Cell>
            <Table.Cell textAlign="center">{price}</Table.Cell>
        </Table.Row>
    )
}

export default CardTableRow