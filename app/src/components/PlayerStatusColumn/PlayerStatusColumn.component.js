import { Grid } from "semantic-ui-react"
import ActionBar from "./ActionBar.component"
import { UserAvatar } from "../User/UserInfo.component"
import EndTurnButton from "./EndTurnButton.component"
import AttackButton from "./AttackButton.component"

import { useSelector } from "react-redux";
import { selectUser, selectOtherUser, selectGame } from "../../core/reducers/game.reducer";
import { useEffect } from "react"
import { useNavigate } from 'react-router-dom';
import { PRIVATE_ROUTES_FULL } from "../../Routes/Routes"

function PlayerStatusColumn() {
  const navigate = useNavigate();

  const user = useSelector(selectUser);
  const otherUser = useSelector(selectOtherUser);
  const room = useSelector(selectGame);

  useEffect(() => {
    if(room.status === "ended") {
      navigate(PRIVATE_ROUTES_FULL.HOME);
    }
  }, [room, navigate])

  return (
    <Grid style={{height: "100%", display: "flex", flexDirection: "column", justifyContent: "center"}}>
      <Grid.Row>
        <UserAvatar name={`${user.id} ${user.surName}`}/>
      </Grid.Row>
        <ActionBar actionPoints={user.actionPoint} />
        <EndTurnButton />
        <AttackButton />
        <ActionBar actionPoints={otherUser.actionPoint} />
      <Grid.Row>  
        <UserAvatar name={`${otherUser.id} ${otherUser.surName}`}/>
      </Grid.Row>  
    </Grid>
  )
}

export default PlayerStatusColumn
