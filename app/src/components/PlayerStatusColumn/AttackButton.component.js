import { Grid, Button } from "semantic-ui-react"

import { useSelector } from "react-redux";
import { selectIsUserTurn, selectedSourceCard, selectedTargetCard } from "../../core/reducers/game.reducer";

import gameService from "../../ws/game";
import { useEffect, useState } from "react";

function AttackButton() {
    const [getIsUserAbleToAttack, setIsUserAbleToAttack] = useState(false);
    const isUserTurn = useSelector(selectIsUserTurn);
    const targetCard = useSelector(selectedTargetCard);
    const sourceCard = useSelector(selectedSourceCard);

    useEffect(() => {
        setIsUserAbleToAttack(isUserTurn && targetCard !== undefined && sourceCard !== undefined);
    }, [isUserTurn, targetCard, sourceCard]);

    const handleOnClick = () => {
        if(!getIsUserAbleToAttack)
            return;
        
        gameService.attack(sourceCard.idCardInstance, targetCard.idCardInstance);
    }


    return (
        <Grid.Row>
            <Button style={{width: "100%"}} primary onClick={handleOnClick} disabled={!getIsUserAbleToAttack}>Attack</Button>
        </Grid.Row>
    )
}

export default AttackButton;
