import { Grid, Progress } from "semantic-ui-react"

function ActionBar({actionPoints}) {
  return (
    <Grid.Row>
        <Progress style={{width: "100%"}} total={200} value={actionPoints} indicating progress="ratio">Action Bar</Progress>
    </Grid.Row>
  )
}

export default ActionBar