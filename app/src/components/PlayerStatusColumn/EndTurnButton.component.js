import { Grid, Button } from "semantic-ui-react"

import { useSelector } from "react-redux";
import { selectIsUserTurn } from "../../core/reducers/game.reducer";

import gameService from "../../ws/game";

function EndTurnButton() {
    const handleOnClick = () => {
        gameService.endTurn();
    }

    const isUserTurn = useSelector(selectIsUserTurn);

    return (
        <Grid.Row>
            <Button style={{width: "100%"}} primary onClick={handleOnClick} disabled={!isUserTurn}>End Turn</Button>
        </Grid.Row>
    )
}

export default EndTurnButton;
