import React from 'react'
import ReactDOM from 'react-dom/client'
import { Routes, Route, BrowserRouter } from 'react-router-dom'
import { disableReactDevTools } from '@fvilers/disable-react-devtools';

import './index.css'
import App from './App'
import { Provider } from 'react-redux';
import { store } from './core/stores';

// import ws from "./ws/index";

const root = ReactDOM.createRoot(document.getElementById('root'));

if (process.env.NODE_ENV === 'production') {
    disableReactDevTools();
}

root.render(
    //<React.StrictMode>
        <Provider store={store}>
            <BrowserRouter basename={process.env.REACT_APP_FRONTEND_BASENAME}>
                <Routes>
                    <Route path="/*" element={<App />} />
                </Routes>
            </BrowserRouter>
        </Provider>
    //</React.StrictMode>
)
