import { socket } from "./index";

import { store } from "../core/stores/index";
import { update } from "../core/reducers/game.reducer";

const joinedCallbacks = {};

socket.on("joined", (room) => {
  store.dispatch(update(room));

  Object.values(joinedCallbacks).forEach(callback => {
    callback(room);
  })
});

socket.on("gameUpdated", (room) => {
  store.dispatch(update(room));
})

const gameService = {
  // rooms
  joinWaitingList(cards = []) {
    socket.emit("joinWaitingList", { cards });
  },
  quitWaitingList() {
    socket.emit("quitWaitingList");
  },
  onJoinedCallback(id, callback) {
    joinedCallbacks[id] = callback;
  },
  onJoinedRemoveCallback(id) {
    delete joinedCallbacks[id];
  },

  //game logic
  endTurn() {
    socket.emit("endTurn");
  },
  attack(idCardSource, idCardTarget) {
    console.log(idCardSource, idCardTarget)
    socket.emit("attack", { idCardSource, idCardTarget });
  }
}

export default gameService;