export const WS = {
  CONNECTION: "connection",
  DISCONNECT: "disconnect",
  LOGIN: "login",
  LOGOUT: "logout",
  NEW_CONNECTED_USER: "newConnectedUser",
  USER_DISCONNECTED: "userDisconnected"
}

export const CHAT = {
  MESSAGE: "message"
}

export const GAME = {
  JOIN_WAITING_LIST: "joinWaitingList",
  QUIT_WAITING_LIST: "quitWaitingList",
  JOINED: "joined",
  ACTION: "action",
  GAME_UPDATED: "gameUpdated"
}