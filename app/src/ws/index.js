import socketIO from 'socket.io-client';

import { store } from "../core/stores/index";
import { addMessage, selectChatPal, addNewUser, removeUser, initConnectedUsers } from "../core/reducers/chat.reducer";
import { getConnectedUsers } from "../api/public/apiChat";
import { selectUserId } from "../core/reducers/user.reducer";
import { getUserPendingTransaction } from '../api/private/apiTransaction';
import { updateUserMoney } from '../api/public/apiUsers';

// export const socket = socketIO.connect('http://localhost/', {path: "/nodejs/socket.io"});
export const socket = socketIO.connect('http://localhost:8080');

socket.on("message", (message) => {
  store.dispatch(addMessage(message));
})

socket.on("newConnectedUser", (userInfo) => {
  store.dispatch(addNewUser(userInfo));
})

socket.on("userDisconnected", (userInfo) => {
  store.dispatch(removeUser(userInfo));
})

window.socket = socket;

const WS = {
  async login(user) {
    socket.emit("login", user);

    const { data } = await getConnectedUsers();
    
    const idUser = selectUserId(store.getState())
    store.dispatch(initConnectedUsers(data));

    if (idUser != null) {
      socket.on(idUser, async (object) => {

        const notification = JSON.parse(object)
        console.log("NOTIFICATION RECEIVED", JSON.parse(object));

        if (notification.notificationAction === "UPDATE") {
          try {
            await getUserPendingTransaction(idUser)
            await updateUserMoney(idUser)
          } catch (error) {
            console.error(error)
          }
        } else {
          //TODO: notification toast
          if(notification.message !== undefined) {
            alert(notification.message)
          }
        }
      })
    }

  },
  sendMessage(text) {
    const destinationId = selectChatPal(store.getState())?.socketId;

    socket.emit("message", {
      destination: destinationId,
      timestamp: new Date(),
      text
    });
  },
}

export default WS