//public routes

export const API_LOGIN_URL = "/auth/login"
export const API_ADD_USER_URL = "/users"
export const API_CONNECTED_USER_URL = "/users"

//private routes
export const API_GET_USERS = (id) => "/users/" + id

// cardInstances
export const API_GET_CARD_INSTANCES_URL = "/cardinstances"
export const API_GET_CARD_INSTANCES_OF_USER = (id) => "/cardinstances/user/" + id
export const API_GET_SELLABLE_CARD_INSTANCES = "/cardinstances/sell"

//card
export const API_GET_ALL_CARDS = "/cards"
export const API_GET_CARD_ID = (id) => "/cards/" + id

//transaction
export const API_TRANSACTION_BUY = "/transactions/buy"
export const API_TRANSACTION_SELL = "/transactions/sell"
export const API_TRANSACTION_PENDING_FOR_USER = (id) => "/transactions/pending/user/" + id

//chat
export const API_CHAT_GET_BY_DEST = (id) => "/chathistory/dest/" + id
export const API_CHAT_GET_BY_SRC = (id) => "/chathistory/src/" + id