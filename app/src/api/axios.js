import axios from 'axios';

const BACKEND_URL = (process.env.REACT_APP_BACKEND_IP || window.location.origin)
const DEFAULT_API_PATH = process.env.REACT_APP_API_BASE_URL


console.log("DEFAULT ROUTE", BACKEND_URL, DEFAULT_API_PATH)
export const axiosPublic =  axios.create({
    baseURL: BACKEND_URL + DEFAULT_API_PATH,
    headers: { 'Content-Type': 'application/json' },
    withCredentials: false
})

export const axiosPrivate = axios.create({
    baseURL: BACKEND_URL + DEFAULT_API_PATH,
    headers: { 'Content-Type': 'application/json' },
    withCredentials: true
})