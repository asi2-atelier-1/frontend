import { axiosPrivate } from "../axios"
import { API_CHAT_GET_BY_DEST, API_CHAT_GET_BY_SRC } from "../apiRoutes"

export async function getChatHistory(userId) {
    try {
        const messagesDest = await axiosPrivate.get(API_CHAT_GET_BY_DEST(userId))
        const messagesSrc = await axiosPrivate.get(API_CHAT_GET_BY_SRC(userId))
        
        return [...messagesDest.data, ...messagesSrc.data]
    } catch (error) {
        console.error(error)
        throw error
    }
}