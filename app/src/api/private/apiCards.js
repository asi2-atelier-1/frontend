import { axiosPrivate } from "../axios"
import { API_GET_CARD_ID, API_GET_CARD_INSTANCES_OF_USER, API_GET_SELLABLE_CARD_INSTANCES } from "../apiRoutes"


export async function getUserCardInstances(id) {
    try {
        const response = await axiosPrivate.get(
            API_GET_CARD_INSTANCES_OF_USER(id)
        )

        if (response?.status === 200) {
            console.log(response)
            return response.data
        }
    } catch (error) {
        console.error(error)
        throw error
    }
}

export async function getUserCardInstancesToSell() {
    try {
        const response = await axiosPrivate.get(
            API_GET_SELLABLE_CARD_INSTANCES
        )

        if (response?.status === 200) {
            console.log(response)
            return response.data
        }
    } catch (error) {
        console.error(error)
        throw error
    }
}

export async function getCardById(id) {
    try {
        const response = await axiosPrivate.get(
            API_GET_CARD_ID(id)
        )

        if (response?.status === 200) {
            return response.data
        }
    } catch (error) {
        console.error(error)
        throw error
    }
}