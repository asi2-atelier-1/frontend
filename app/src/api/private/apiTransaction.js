import { axiosPrivate } from "../axios"
import { API_TRANSACTION_BUY, API_TRANSACTION_PENDING_FOR_USER, API_TRANSACTION_SELL } from "../apiRoutes"
import { store } from "../../core/stores"
import { updateTransactionPending } from "../../core/reducers/cardTransaction.reducer"

async function postTransaction(cardInstanceId, userId, isBuy) {
    const URL = isBuy ? API_TRANSACTION_BUY : API_TRANSACTION_SELL
    try {
        const response = await axiosPrivate.post(
            URL,
            JSON.stringify({
                "userId": userId,
                "cardInstanceId": cardInstanceId
            })
        )

        if (response?.status === 200) {
            console.log(response)
            return response.data
        }
    } catch (error) {
        throw error
    }
}

export async function sellCard(cardInstanceId, userId) {
    return await postTransaction(cardInstanceId, userId, false)
}
export async function buyCard(cardInstanceId, userId) {
    return await postTransaction(cardInstanceId, userId, true)
}

export async function getUserPendingTransaction(userId) {
    if (userId == null) return

    try {
        const response = await axiosPrivate.get(
            API_TRANSACTION_PENDING_FOR_USER(userId)
        )

        if (response?.status === 200) {
            store.dispatch(updateTransactionPending(response.data))
        }
    } catch (error) {
        console.error(error)
    }

}