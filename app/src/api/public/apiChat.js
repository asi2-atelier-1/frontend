import axios from "axios"
import { API_CONNECTED_USER_URL } from "../apiRoutes"

export async function getConnectedUsers() {
  try {
    return await axios.get("http://localhost:8080" + API_CONNECTED_USER_URL);
  } catch (error) {
    console.error(error)
    if (error?.response?.data) {
      throw new Error("Error while creating user")
    }

    throw error
  }
}