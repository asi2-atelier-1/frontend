import { axiosPublic } from "../axios"
import { API_ADD_USER_URL, API_GET_USERS } from "../apiRoutes"
import { store } from "../../core/stores/index"
import { updateAccount } from "../../core/reducers/user.reducer"

export async function addUser(email, login, password, account, surName, lastName) {
    try {
        const response = await axiosPublic.post(
            API_ADD_USER_URL,
            JSON.stringify({
                "email": email,
                "login": login,
                "pwd": password,
                "account": account,
                "surName": surName,
                "lastName": lastName
            })
        )
    
        if (response?.status === 200) {
            
        }
        
    } catch (error) {
        console.error(error)

        throw error
    }
    
}

export async function updateUserMoney (userId) {
    if (userId == null) return
    try {
        const response = await axiosPublic.get(
            API_GET_USERS(userId)
        )
    
        if (response?.status === 200) {
            store.dispatch(updateAccount(response.data.account))
        }
        
    } catch (error) {
        console.error(error)
    }
    
}