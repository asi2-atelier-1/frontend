import { axiosPublic } from "../axios"
import { API_LOGIN_URL } from "../apiRoutes"
import { store } from '../../core/stores'
import { login as loginAction } from '../../core/reducers/user.reducer'
import { initMessages } from '../../core/reducers/chat.reducer'
import ws from "../../ws/index"
import { getChatHistory } from "../private/apiChat";

// function getRandomInt(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min + 1)) + min;
// }

export async function login (email, password) {
    try {
        const response = await axiosPublic.post(
            API_LOGIN_URL,
            JSON.stringify({
                "email": email,
                "pwd": password
            }),
        )
        
        if (response?.status === 200) {
            console.log(response)
            store.dispatch(loginAction({...response.data}))
            getChatHistory(response.data.id).then(messages => {
                store.dispatch(initMessages(messages))
            })
            ws.login(response.data);
        }
        // const user = {
        //     id: getRandomInt(0, 100),
        //     surName: "test",
        //     lastName: "test",
        //     email: "test@test.fr",
        //     login: "test",
        //     token: "fsdfart re.waerawer",
        //     account: 120
        // };
        // store.dispatch(loginAction(user));
    } catch (error) {

        if (error?.response?.data) {
            const errorMessage = error.response.data.message
            throw new Error(errorMessage)
        }
        
        throw error
    }
}
