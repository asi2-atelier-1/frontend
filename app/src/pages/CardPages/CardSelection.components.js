import { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { Button, Grid, Header, Segment } from "semantic-ui-react"
import { PRIVATE_ROUTES_FULL } from "../../Routes/Routes"

import { getUserCardInstances } from "../../api/private/apiCards"
import CardPreviewSmall from "../../components/Cards/CardPreviewSmall.component"
import { selectUserId } from "../../core/reducers/user.reducer"
import gameService from "../../ws/game"

function CardSelection() {

    const id = useSelector(selectUserId)
    const navigate = useNavigate()
    const [selectedCardList, setselectedCardList] = useState({})
    const [cardInstanceList, setCardInstanceList] = useState(null)
    
    useEffect(() => {
        const getCardInstanceList = async (id) => {
            try {
                const response = await getUserCardInstances(id)
                if (response !== null) {
                    setCardInstanceList(response)
                } else {
                    setCardInstanceList({})
                }
            } catch (error) {
                console.log(`Error : ${error}`)
            }
        }

        getCardInstanceList(id)
    }, [id])

    // Add or remove clicked card from selected cards list
    const handleOnClick = (card) => {
        if (!isCardSelected(card) && Object.keys(selectedCardList).length < 4) {
            setselectedCardList({
                ...selectedCardList,
                [card.idCardInstance]: {
                    "cardInstance": card
                }
            })
        } else {
            const prev = {...selectedCardList}
            delete prev[card.idCardInstance]
            setselectedCardList(prev)
        }
    }

    const handleFightOnClick = () => {
        gameService.joinWaitingList(selectedCardList)
        navigate(PRIVATE_ROUTES_FULL.WAIT)
    }

    // Check if the clicked card is alredy selected
    const isCardSelected = (card) => {
        return card.idCardInstance in selectedCardList
    }

    if (cardInstanceList === null)
        return null

    return (
        <Segment placeholder>
            <Grid columns={2} relaxed='very' stackable>
                <Grid.Row style={{justifyContent: "center"}}>
                    <Header as="h2">Select you cards for the battle</Header>
                </Grid.Row>
                <Grid.Row>
                {Object.keys(selectedCardList).length === 4 
                    ? <Button content="Fight !" onClick={handleFightOnClick} primary/> 
                    : <Button 
                        content={`${Object.keys(selectedCardList).length} / 4`} 
                        secondary />}
                </Grid.Row>
                <Grid.Row>
                        {cardInstanceList.map(card => {
                            return <Grid.Column key={card.idCardInstance} style={{marginBottom: "2em"}} width={4}>
                                <CardPreviewSmall
                                    // key={card.idCardInstance}
                                    cardInstance={card}
                                    isSelected={isCardSelected(card)}
                                    handleOnClick={handleOnClick}
                                />
                            </Grid.Column>
                        })}
                </Grid.Row>
                
            </Grid>
        </Segment>
    )
}

export default CardSelection