import { Route, Routes } from "react-router-dom"

import { PRIVATE_ROUTES_RELATIVE } from "../../Routes/Routes"
import NotFound from "../NotFound.page"
import CardPageTable from "./CardPageTable.component"

function CardPageRouter() {
    return (
        <Routes>
            <Route
                path={PRIVATE_ROUTES_RELATIVE.CARDS.BUY}
                element={<CardPageTable isBuy={true} />}
            />
            <Route
                path={PRIVATE_ROUTES_RELATIVE.CARDS.SELL}
                element={<CardPageTable isBuy={false}/>}
            />

            {/* Not Found Route */}
            <Route path="*" element={<NotFound />} />
        </Routes>
    )
}

export default CardPageRouter