import { useEffect, useMemo, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Grid, Header, Loader, Message, Segment, Table } from "semantic-ui-react"

import { getUserCardInstances, getUserCardInstancesToSell } from "../../api/private/apiCards"
import { buyCard, sellCard } from "../../api/private/apiTransaction"
import CardPreview from "../../components/Cards/CardPreview.component"
import CardTableHeader from "../../components/Cards/CardTableHeader.component"
import CardTableRow from "../../components/Cards/CardTableRow.component"
import { cardTransactionPending, initCardList, selectSelectedCardBuy, selectSelectedCardBuyPending, selectSelectedCardSell, selectSelectedCardSellPending, selectToBuyCardList, selectToSellCardList, setSelectedCard } from "../../core/reducers/cardTransaction.reducer"
import { selectUserId } from "../../core/reducers/user.reducer"

function CardPageTable({isBuy}) {

    const id = useSelector(selectUserId)
    const cardInstanceList = useSelector(isBuy ? selectToBuyCardList : selectToSellCardList)
    const transactionType = useMemo(() => isBuy ? "BUY" : "SELL", [isBuy])
    
    const selectedCard = useSelector(isBuy ? selectSelectedCardBuy : selectSelectedCardSell)
    const selectedCardPending = useSelector(isBuy ? selectSelectedCardBuyPending : selectSelectedCardSellPending)
    
    const [apiState, setApiState] = useState({
        isLoading: false,
        error: null
    })

    const { isLoading, error} = apiState
    const dispatch = useDispatch()
    const handleOnClick = (id) => {
        const card = cardInstanceList?.filter(card => card.idCardInstance === id)
        if (card[0]) {
            dispatch(setSelectedCard(id, transactionType))
        }
    }

    const handleCardTransaction = async () => {
        const idCardInstance = selectedCard.idCardInstance
        // const price = isBuy ? -1 * selectedCard.price : selectedCard.price
        try {
            if (! selectedCard.isPending) {
                if (isBuy) await buyCard(idCardInstance, id)
                else await sellCard(idCardInstance, id)
                dispatch(cardTransactionPending(idCardInstance, transactionType))
            }
        } catch (error) {
            throw error
        }
    } 
    
    useEffect(() => {
        const getCardInstanceList = async (id) => {
            try {
                setApiState({isLoading: true, error: null})
                const response = !isBuy ? await getUserCardInstances(id) : await getUserCardInstancesToSell()
                if (response !== null) {
                    dispatch(initCardList(response, transactionType))
                    setApiState({isLoading: false, error: null})
                }
            } catch (error) {
                setApiState({isLoading: false, error: error})
            }
        }

        getCardInstanceList(id)
    }, [id, isBuy, dispatch, transactionType])

    if (isLoading) {
        return <Loader active inline>Loading...</Loader>
    } else {
        if (error) {
            return <Message error>Error while loading list of Cards</Message>
        } else if (cardInstanceList !== null) {
            const headerText = isBuy ? "Buy a card" : "Sell your cards"
            const priceText = transactionType
            return (
                <Segment placeholder>
                    <Grid columns={2} relaxed='very' stackable>
                        <Grid.Row style={{justifyContent: "center"}}>
                            <Header as="h2">{headerText}</Header>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column width={12}>
                                <Table celled>
                                    <CardTableHeader />
                    
                                    <Table.Body>
                                        {cardInstanceList.map(card =>
                                            <CardTableRow
                                                key={card.idCardInstance}
                                                id={card.idCardInstance}
                                                attack={card.attack}
                                                defence={card.defence}
                                                energy={card.energy}
                                                hp={card.hp}
                                                price={card.price}
                                                handleOnClick={handleOnClick}
                                            />
                                        )}
                                    </Table.Body>
                    
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                {selectedCard &&
                                    <CardPreview
                                        cardInstance={selectedCard}
                                        isPending={selectedCardPending}
                                        priceText={priceText}
                                        handleCardTransaction={handleCardTransaction}
                                    />               
                                }
                            </Grid.Column>
                        </Grid.Row>
                        
                    </Grid>
                </Segment>
            )
        } else {
            return null
        }
    }

}

export default CardPageTable