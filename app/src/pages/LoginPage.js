import { useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'

import { PUBLIC_ROUTES_FULL } from "../Routes/Routes"
import { login } from "../api/public/apiLogin"


function LoginPage() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const [errorMessage, setErrorMessage] = useState("")

    const handleForm = () => {
        try {
            login(email, password)
            setErrorMessage("")
        } catch (error) {
            setErrorMessage(error.message)
        }
    }

    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='black' textAlign='center'>
                    <Image src='./images/card-game.svg' /> Log-in to your account
                </Header>
                <Form size='large'>
                    <Segment stacked>
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='E-mail address'
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            onChange={(e) => setPassword(e.target.value)}
                        />

                        <Button color='blue' fluid size='large' onClick={handleForm}>
                            Login
                        </Button>
                    </Segment>
                </Form>
                {
                    errorMessage !== ""
                    ?
                    <Message success={false} visible={errorMessage !== ""}>
                        {errorMessage}
                    </Message>
                    : null
                }
                <Message>
                    New to us? <Link to={PUBLIC_ROUTES_FULL.SIGN_UP}>Sign Up</Link>
                </Message>
            </Grid.Column>
        </Grid>
    )
}

export default LoginPage