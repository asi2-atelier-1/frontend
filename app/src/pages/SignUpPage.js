import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'

import { PUBLIC_ROUTES_FULL } from "../Routes/Routes"
import { addUser } from '../api/public/apiUsers'

function SignUpPage() {

    const [email, setEmail] = useState("")
    const [login, setLogin] = useState("")
    const [password, setPassword] = useState("")
    const [surName, setSurName] = useState("")
    const [lastName, setLastName] = useState("")
    const [account, setAccount] = useState(0)

    const navigate = useNavigate()
    const [success, setSuccess] = useState(null)
    const successMessage = success ? "The user was successfully created, you'll be redirected in 3s" : "There was a problem while creating your user"
    const handleForm = async () => {
        try {
            await addUser(email, login, password, account, surName, lastName)
            setSuccess(true)
        } catch (error) {
            setSuccess(false)
        }
    }

    useEffect(() => {
        const timeout = setTimeout(() => {
            if (success) {
                navigate(PUBLIC_ROUTES_FULL.LOGIN)
            }
        }, 3 * 1000)

        return () => clearTimeout(timeout)
    }, [success, navigate])

    //TODO add validation
    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='primary' textAlign='center'>
                    <Image src='./images/card-game.svg' /> Create a new account
                </Header>
                <Form size='large'>
                    <Segment stacked>
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='E-mail address'
                            type="email"
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='Login'
                            onChange={(e) => setLogin(e.target.value)}
                        />
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        {/* <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Verify Password'
                            type='password'
                        /> */}
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='surName'
                            onChange={(e) => setSurName(e.target.value)}
                        />
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='lastName'
                            onChange={(e) => setLastName(e.target.value)}
                        />
                        <Form.Input
                            fluid
                            icon='dollar'
                            iconPosition='left'
                            placeholder='Initial amount of your account'
                            type="number"
                            onChange={(e) => setAccount(e.target.value)}
                        />

                        <Button color='primary' fluid size='large' onClick={handleForm} disabled={success}>
                            Sign Up
                        </Button>
                    </Segment>
                </Form>
                {
                    success !== null
                    ?
                    <Message success={success} error={!success}>
                        {successMessage}
                    </Message>
                    : null
                }
                <Message>
                    Go back to <Link to={PUBLIC_ROUTES_FULL.LOGIN}>login page</Link>
                </Message>
            </Grid.Column>
        </Grid>
    )
}

export default SignUpPage