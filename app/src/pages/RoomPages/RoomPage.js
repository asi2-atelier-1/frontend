import { Grid } from 'semantic-ui-react'
import Chat from '../../components/Chat/Chat.component'
import PlayerStatusColumn from '../../components/PlayerStatusColumn/PlayerStatusColumn.component'
import Board from '../../components/Board/Board.components'

import BoardCardPreview from '../../components/Board/BoardCardPreview.component'

function RoomPage() {
  return (
    <Grid style={{padding : "0.7em"}} columns='equal'>
      <Grid.Row>
        <Grid.Column width={3}>
          <Chat />
        </Grid.Column>
        <Grid.Column width={2}>
          <PlayerStatusColumn />
        </Grid.Column>
        <Grid.Column  width={8}>
          <Board/>
        </Grid.Column>
        <Grid.Column width={3}>
          <BoardCardPreview />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

RoomPage.propTypes = {}

export default RoomPage
