import { Route, Routes } from "react-router-dom"

import { PRIVATE_ROUTES_RELATIVE } from "../../Routes/Routes"
import CardSelection from "../CardPages/CardSelection.components"
import NotFound from "../NotFound.page"
import RoomMatchmaking from "./RoomMatchmaking"
import RoomPage from "./RoomPage"

function RoomPageRouter() {
    return (
        <Routes>
            <Route index element={<CardSelection />} />
            <Route
                path={PRIVATE_ROUTES_RELATIVE.PLAY.WAIT}
                element={<RoomMatchmaking />}
            />
            <Route
                path={PRIVATE_ROUTES_RELATIVE.PLAY.GAME}
                element={<RoomPage />}
            />


            {/* Not Found Route */}
            <Route path="*" element={<NotFound />} />
        </Routes>
    )
}

export default RoomPageRouter