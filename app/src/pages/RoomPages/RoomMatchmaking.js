import { Loader, Grid } from 'semantic-ui-react';
import { useEffect } from "react"
import game from "../../ws/game";
import { useNavigate } from 'react-router-dom';
import { PRIVATE_ROUTES_FULL } from '../../Routes/Routes';

function RoomMatchmaking() {

  const navigate = useNavigate();
  useEffect(() => {
    game.onJoinedCallback("matchmaking", (room) => {
      navigate(PRIVATE_ROUTES_FULL.GAME(room.id))
    });


    return () => {
      game.quitWaitingList();
      game.onJoinedRemoveCallback("matchmaking");
    };
  }, [navigate]);

  return (
    <Grid style={{ height: "80vh" }}>
      <Grid.Row>
        <Grid.Column>
          <Loader active size='massive' style={{ width: "100%" }}>Looking for opponents</Loader>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

export default RoomMatchmaking
