import { Button, Table } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { PRIVATE_ROUTES_FULL } from '../../Routes/Routes'

const TableHeader = () => (
    <Table.Header>
        <Table.Row>
        <Table.HeaderCell>Room</Table.HeaderCell>
        <Table.HeaderCell>Host</Table.HeaderCell>
        <Table.HeaderCell>Join !</Table.HeaderCell>
        </Table.Row>
    </Table.Header>
)

const TableRow = ({roomName, hostName, link}) => (
    <Table.Row>
        <Table.Cell>{roomName}</Table.Cell>
        <Table.Cell>{hostName}</Table.Cell>
        <Table.Cell>
            <Link to={link}>
                <Button>Click Here</Button>
            </Link>
        </Table.Cell>
    </Table.Row>
)

function RoomListPage({roomList}) {
    if (roomList == null) return null
    return (
    <Table celled>
    <TableHeader/>

    <Table.Body>
        {roomList.map(r => <TableRow roomName={r.roomName} hostName={r.hostName} link={r.link} key={r.id}/>)}
    </Table.Body>
  </Table>
  )
}

RoomListPage.propTypes = {}

export default RoomListPage
