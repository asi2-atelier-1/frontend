import { Button } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { PRIVATE_ROUTES_FULL } from '../Routes/Routes'

const ButtonImage = ({ icon, link, content }) => (
    <Link to={link}>
        <Button
            size="massive"
            content={content}
            icon={icon}
            labelPosition='left'
        />
    </Link>
)

function HomePage() {
    return (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "100vh"

            }}
        >

            <div 
                style={
                    {
                        display: "grid",
                        gridTemplateRows: "1fr 1fr",
                        gridTemplateColumns: "1fr 1fr",
                        rowGap: "30px",
                        columnGap: "15px"
                    }
                }
            >
                
                <div style={{justifySelf: "end"}}>
                    <ButtonImage
                        icon='dollar'
                        link={PRIVATE_ROUTES_FULL.BUY}
                        content='BUY'
                    />
                </div>
                <div style={{justifySelf: "start"}}>
                    <ButtonImage
                        icon='dollar'
                        link={PRIVATE_ROUTES_FULL.SELL}
                        content='SELL'
                    />
                </div>
                <div style={{gridColumn: "1 / span 2", justifySelf: "center"}}>
                    <ButtonImage
                        icon='gamepad'
                        link="/rooms/"
                        content='PLAY'
                    />
                </div>
            </div>
        </div>
    )
}


export default HomePage
