import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { PRIVATE_ROUTES_FULL } from '../Routes/Routes';

function NotFound() {
    const navigate = useNavigate()

    useEffect( () => {
        navigate(PRIVATE_ROUTES_FULL.HOME, {replace: true})
    })

    return null
}

export default NotFound