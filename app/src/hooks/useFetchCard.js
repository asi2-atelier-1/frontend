import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getCardById } from "../api/private/apiCards"
import { addCard, selectCardById } from "../core/reducers/card.reducer"

const useFetchCard = (id) => {
    
    const card = useSelector(selectCardById(id))
    const dispatch = useDispatch()

    useEffect(() => {

        const getCard = async (id) => {
            try {
                const response = await getCardById(id)
                dispatch(addCard(response))
            } catch(e) {
                console.error(`💔 Card API ERROR : ${e}`)
            }
        }

        if (card == null) {
            getCard(id)
        }

    }, [card, dispatch, id])

    return card;
    
}

export default useFetchCard