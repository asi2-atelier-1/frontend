import { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { useSelector } from "react-redux"

import { axiosPrivate } from "../api/axios"
import { PRIVATE_ROUTES_FULL } from "../Routes/Routes"
import { selectUserToken } from "../core/reducers/user.reducer"

const useAxiosPrivate = () => {
    const navigate = useNavigate()
    const token = useSelector(selectUserToken)
    useEffect(() => {

        const requestIntercept = axiosPrivate.interceptors.request.use(
            config => {
                if (!config.headers['Authorization']) {
                    config.headers['Authorization'] = `Bearer ${token}`
                }
                return config;
            }, (error) => Promise.reject(error)
        )

        const responseIntercept = axiosPrivate.interceptors.response.use(
            response => response,
            async (error) => {
                const prevRequest = error?.config;
                if (error?.response?.status === 401 && !prevRequest?.sent) {
                    prevRequest.sent = true
                    // prevRequest.headers['Authorization'] = `Bearer ${token}`
                    // return axiosPrivate(prevRequest)
                    navigate(PRIVATE_ROUTES_FULL.HOME, {replace: true})
                }
                return Promise.reject(error);
            }
        )

        return () => {
            axiosPrivate.interceptors.request.eject(requestIntercept);
            axiosPrivate.interceptors.response.eject(responseIntercept);
        }
    }, [navigate])

    return axiosPrivate
}

export default useAxiosPrivate