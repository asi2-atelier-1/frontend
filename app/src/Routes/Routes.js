export const PUBLIC_ROUTES_FULL = {
    "LOGIN": "/login",
    "SIGN_UP": "/signup"
}

export const PUBLIC_ROUTES_RELATIVE = {
    "index" : "/",
    "LOGIN": "login",
    "SIGN_UP": "signup"
}

export const PRIVATE_ROUTES_FULL = {
    "HOME": "/",
    "BUY": "/cards/buy",
    "SELL": "/cards/sell",
    "PLAY": "/rooms",
    "WAIT": "/rooms/waiting",
    "GAME": (id) => `/rooms/${id}`
}

export const PRIVATE_ROUTES_RELATIVE = {
    "index": "/",
    "CARDS": {
        "index": "cards/*",
        "BUY": "buy",
        "SELL": "sell"
    },
    "PLAY": {
        "index": "rooms/*",
        "WAIT": "waiting",
        "GAME": ":id"
    }
} 