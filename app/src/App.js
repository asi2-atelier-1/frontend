import { Routes, Route } from 'react-router-dom';

import './App.css'
import 'semantic-ui-css/semantic.min.css'

import RequireAuth from './components/Auth/RequireAuth.component'
import RequireNoAuth from './components/Auth/RequireNoAuth.component'
import Layout from './components/Layout.component'
import CardPageRouter from './pages/CardPages/CardPageRouter.component'
import RoomPageRouter from './pages/RoomPages/RoomPageRouter.component'
import NotFound from './pages/NotFound.page'
import { PRIVATE_ROUTES_RELATIVE, PUBLIC_ROUTES_RELATIVE } from './Routes/Routes'

import LoginPage from './pages/LoginPage'
import SignUpPage from './pages/SignUpPage'
import HomePage from './pages/HomePage'

function App() {


    return (
        <Routes>
            <Route path={PUBLIC_ROUTES_RELATIVE.index} element={<Layout />} >
                {/* public routes */}
                <Route element={<RequireNoAuth />} >
                    <Route path={PUBLIC_ROUTES_RELATIVE.LOGIN} element={<LoginPage auth={false}/>} />
                </Route>
                <Route element={<RequireNoAuth />} >
                    <Route path={PUBLIC_ROUTES_RELATIVE.SIGN_UP} element={<SignUpPage />} />
                </Route>

                {/* Protected Routes */}
                <Route element={<RequireAuth />} >
                    <Route index element={<HomePage />} />
                </Route>

                <Route element={<RequireAuth />} >
                    <Route path={PRIVATE_ROUTES_RELATIVE.CARDS.index} element={<CardPageRouter />}/>
                </Route>

                <Route element={<RequireAuth />} >
                    <Route path={PRIVATE_ROUTES_RELATIVE.PLAY.index} element={<RoomPageRouter />} />
                </Route>

                {/* Not Found Route */}
                <Route path="*" element={<NotFound />} />
            </Route>
        </Routes>
    )
}

export default App
